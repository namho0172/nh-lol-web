const state = () => ({
    globalMenu: [
        {
            parentName: 'HOME',
            icon: 'el-icon-tickets',
            menuLabel: [
                { id: 'DASH_BOARD', currentName: '대시보드', link: '/', isShow: true },
            ]
        },
        {
            parentName: '회원관리',
            icon: 'el-icon-tickets',
            menuLabel: [
                { id: 'MEMBER_ADD',  currentName: '회원등록', link: '/', isShow: true },
                { id: 'MEMBER_EDIT',  currentName: '회원수정', link: '/', isShow: true },
                { id: 'MEMBER_LIST',  currentName: '회원리스트', link: '/customer/list', isShow: true },
                { id: 'MEMBER_DETAIL',  currentName: '회원상세정보', link: '/', isShow: true },
            ]
        },
        {
            parentName: '마이 메뉴',
            icon: 'el-icon-tickets',
            menuLabel: [
                { id: 'MEMBER_LOGOUT',  currentName: '로그아웃', link: '/my-menu/logout', isShow: true },
            ]
        },
    ],
    selectedMenu: 'DASH_BOARD'
})

export default state
